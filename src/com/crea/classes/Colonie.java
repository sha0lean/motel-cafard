package com.crea.classes;

import java.util.Observable;
import java.util.Observer;

public class Colonie implements Observer {
    private String nom;
    private Integer population;
    private Integer taux;

    public Colonie ( String nom, Integer population,Integer taux ) {
        this.nom = nom;
        this.population = population;
        this.taux = taux;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public Integer getTaux() {
        return taux;
    }

    public void setTaux(Integer taux) {
        this.taux = taux;
    }

    @Override
    public String toString() {
        return  "Colonie [" + nom +" "+ population +" "+ taux +"]";
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Notifier");
    }
}
