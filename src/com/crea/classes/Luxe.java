package com.crea.classes;

public class Luxe extends Chambre{
    int prix = 75;
    int no;

    public Luxe(int no) {
        this.no = no;
    }

    @Override
    public int getNo() {
        return no;
    }

    @Override
    public void setNo(int no) {
        this.no = no;
    }
}
