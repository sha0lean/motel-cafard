package com.crea.classes;

import java.util.ArrayList;
import java.util.Observable;

public class Motel extends Observable {

    private static Motel motel;
    int capacite = 0;
    ArrayList<Chambre> listChambres = createChambres();
    ArrayList<Colonie> listColonies = new ArrayList<>();
    ArrayList<> listColonies = new ArrayList<>();

    private Motel() { }

    /**
     * Singleton de motel
     * @return Instance de Motel
     */
    public static Motel getInstance(){
        if (motel == null){
            motel = new Motel();
        }
        return motel;
    }

    public ArrayList<Chambre> createChambres() {

        ArrayList<Chambre> listCh = new ArrayList<>();

        for (int i = 101; i <= 102; i++) {
            Standard standard = new Standard(i);
            listCh.add(standard);
        }
        for (int i = 103; i <= 104; i++) {
            Luxe luxe = new Luxe(i);
            listCh.add(luxe);
        }
        for (int i = 105; i <= 106; i++) {
            Suite suite = new Suite(i);
            listCh.add(suite);
        }
        return listCh;
    }

    /**
     * Lorsqu'une nouvelle colonie arrive avec un nombre de personne, la valeur s'incrémente avec le nouvel effectif
     * @param capacite du motel
     */
    public void newClient (int capacite) {
        // ajouter colonie a une chambre

        this.capacite+=capacite;
    }

    /**
     * Lorsqu'une colonie part, la capacité générale réduit en fonction
     * @param capacite
     */
    public void removeClient (int capacite) {
        this.capacite-=capacite;
        if(this.capacite >= 0){
            System.out.println("");
        }
    }

    // TO DO
    // attribuer une chambre à une colonie
    //

    private void logerColonie (){

    }

    /**
     * Retourne les chambres disponible à un moment T
     * @return str
     */
    private String getChambres(){
        String str = "";
        for (int i = 0 ; i < listChambres.size(); i++ ){

            if (listChambres.get(i).getDispo()){
                str += String.valueOf(listChambres.get(i).getNo()) + " DISPO \n";
            }
            else {
                str += String.valueOf(listChambres.get(i).getNo()) + " NON DISPO \n";
            }

        }
        return str;
    }

    public String toString() {
        return "Bienvenue dans mon Motel ! \n" + "Chambres : [ \n" + getChambres() + "]";
    }

}
