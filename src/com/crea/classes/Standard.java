package com.crea.classes;

import java.util.ArrayList;

public class Standard extends Chambre {
    int prix = 50;
    int no;
    ArrayList<Comodite> listComodite = new ArrayList<>();

    public Standard(int no) {
        this.no = no;
    }

    @Override
    public int getNo() {
        return no;
    }

    @Override
    public void setNo(int no) {
        this.no = no;
    }

}
