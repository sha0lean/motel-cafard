package com.crea.classes;

import java.util.ArrayList;

public abstract class Chambre {

    int no = 0;
    int prix = 0;
    Boolean dispo = true;

    ArrayList<Comodite> listComodite = new ArrayList<>();

    abstract void setNo(int no);
    abstract int getNo();

    public Boolean getDispo() {
        return dispo;
    }

    public void setDispo(Boolean dispo) {
        this.dispo = dispo;
    }

    public void addComodite(Comodite comodite) {
        listComodite.add(comodite);
    }

    int getPrix() {
        int total = prix;
        for (int i = 0 ; i < listComodite.size() ; i++ ){
            total += listComodite.get(i).getPrix();
        }
        return total;
    };
}
