package com.crea.classes;

public class Suite extends Chambre{
    int prix = 100;
    int no;

    public Suite(int no) {
        this.no = no;
    }

    @Override
    public int getNo() {
        return no;
    }

    @Override
    public void setNo(int no) {
        this.no = no;
    }

}
