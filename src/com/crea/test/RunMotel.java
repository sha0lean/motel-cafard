package com.crea.test;

import com.crea.classes.*;

public class RunMotel {
    public static void main(String[] args) {
        Motel motel = Motel.getInstance();
        motel.createChambres();

        System.out.println(motel);

        Standard ch1 = new Standard(200);

        DoucheRes do1 = new DoucheRes();
        Minibar mi1 = new Minibar();
        Spa como1 = new Spa();

        ch1.addComodite(como1);

        Colonie c1 = new Colonie( "les zeubs", 100, 200);
        Colonie c2 = new Colonie( "tah les oufs", 200, 150);
        Colonie c3 = new Colonie( "les partouzeurs", 50, 420);
        Colonie c4 = new Colonie( "les zeubzzzs", 100, 200);
        Colonie c5 = new Colonie( "tah les oufzzzs", 200, 150);
        Colonie c6 = new Colonie( "les partouzeurzzzs", 50, 420);
        Colonie c7 = new Colonie( "les reniés", 50, 4000);
        //

        motel.addObserver(c1);
        motel.notifyObservers();

        System.out.println(c1);
    }
}
